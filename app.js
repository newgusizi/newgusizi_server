const express = require('express');
const https = require('https');
const bodyParser = require('body-parser');
const dotenv = require("dotenv").config();
const fs = require('fs');
const app = express();
const port = 3651;

const SSL_KEY_PATH = process.env.SSL_KEY_PATH;
const SSL_CERT_PATH = process.env.SSL_CERT_PATH;
const SSL_CA_PATH = process.env.SSL_CA_PATH;
const SSL_CERT_PASSWORD = process.env.SSL_CERT_PASSWORD;

// 2022.02.28 만료
// win10 인증서 발급 참고
// https://blog.itcode.dev/posts/2021/08/19/lets-encrypt
const server_option = {
    key: fs.readFileSync(SSL_KEY_PATH),
    cert: fs.readFileSync(SSL_CERT_PATH),
    ca: fs.readFileSync(SSL_CA_PATH),
    passphrase: SSL_CERT_PASSWORD
};


app.set('views', __dirname + '/views')
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);


//router
const static = require('./router/static_router');
const member = require('./router/member_router');
const freeboard = require('./router/freeboard_router');
const comment = require('./router/comment_router');
const prices = require('./router/prices_router');
const myinfo = require('./router/myinfo_router');
const menu = require('./router/menu_router');
const connect = require('./router/connect_router');
const state = require('./router/state_router');
const portfolio = require('./router/portfolio_router');
const test = require('./router/test_router');

app.use('/static', express.static(__dirname + '/static'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());


app.use('/member', member);
app.use('/prices', prices);
app.use('/freeboard', freeboard);
app.use('/comment', comment);
app.use('/myinfo', myinfo);
app.use('/menu', menu);
app.use('/history', connect);
app.use('/state', state);
app.use('/static', static);
app.use('/portfolio', portfolio);
app.use('/test', test);


https.createServer(server_option, app).listen(port, () => {
    console.log(`server start port: ${port}`);
});