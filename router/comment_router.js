const express = require('express');
const db = require('../db/db_connect');
const router = express.Router();


router.post('/list', async (req, res) => {
    console.log(`comment boardId: ${req.body.boardId}`);
    const result = await db.commentList(req.body.boardId);

    res.send(result);
});


router.post('/insert', async (req, res) => {
    console.log(`comment boardId: ${req.body.boardId}`);
    const result = await db.insertComment(req.body.boardId, req.body.user_id, req.body.comment);

    res.send({cnt: result.affectedRows});
});


module.exports = router;