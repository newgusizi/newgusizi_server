const express = require('express');
const db = require('../db/db_connect');
const router = express.Router();


/**
 * return JSON({cnt:Int})
 */
 router.post('/checkId', async (req, res) => {
    const id = req.body.user_id;
    console.log(`check id :: ${id}`);
    let result;

    try {
        result = await db.checkId(req.body.id);
        console.log(result);
    } catch(e) {
        console.log(`id check failed:: ${req.body.id}`);
        result = {cnt: -1};
    } finally {
        res.send(result);
    }
});


/**
 * return JSON({cnt:Int})
 */
 router.post('/key', async (req, res) => {
    let id = req.body.user_id;
    console.log(`check id :: ${id}`);
    let result;

    try {
        result = await db.getKey(id);
        console.log(result);
    } catch(e) {
        console.log(`id check failed:: ${id}`);
        result = {cnt: -1};
    } finally {
        res.send(result);
    }
});


/**
 * return JSON({cnt:Int})
 */
router.post('/reg', async (req, res) => {
    let user_id = req.body.user_id;
    let user_pw = req.body.user_pw;
    let user_name = req.body.user_name;
    let user_phone = req.body.user_phone;
    let user_key = req.body.user_key;
    console.log(`id:: ${user_id} // pw:: ${user_pw} // name:: ${user_name} // phone:: ${user_phone} // key:: ${user_key}`);

    const result = await db.regMember(user_id, user_pw, user_phone, user_name, user_key);
    res.send({cnt: result.affectedRows});
});


/**
 * return JSON({cnt:Int})
 */
router.post('/login', async (req, res) => {
    console.log(`login ${req.body.user_id} // ${req.body.user_pw}`);

    let result;
    try {
        result = await db.login(req.body.user_id, req.body.user_pw);
    } catch (e) {
        console.log(`err ==> id: ${req.body.user_id} / msg: ${e}`);
        result = {cnt: -1};
    } finally {
        console.log(`finally login result`);
        console.log(result);
        res.send(result);
    }
});


router.put('/cnt/post', async (req, res) => {
    console.log(`/write/post ${req.body.user_id}`);
    await db.addWritePostCnt(req.body.user_id);
});


router.put('/cnt/comment', async (req, res) => {
    console.log(`/write/post ${req.body.user_id}`);
    await db.addWriteCommentCnt(req.body.user_id);
});


router.post('/block', async (req, res) => {
    console.log(`user_id:: ${req.body.user_id}`);
    console.log(`block user_id:: ${req.body.block_user_id}`);
    const result = await db.block_user(req.body.user_id, req.body.block_user_id);
    res.send({cnt: result.affectedRows});
});


module.exports = router;