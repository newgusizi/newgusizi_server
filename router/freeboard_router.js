const express = require('express');
const db = require('../db/db_connect');
const router = express.Router();


router.post('/list', async (req, res) => {
    console.log(`freeboard req info ${req.body.page}`);
    const page = parseInt(req.body.page) ?? 1;
    const result = await db.boardList(page);

    res.send(result);
});


router.post('/detail', async (req, res) => {
    console.log(`free board id:: ${req.body.boardId}`);
    const result = await db.boardDetail(req.body.boardId);

    res.send(result);
});


router.post('/insert', async (req, res) => {
    console.log(`free board insert id:: ${req.body.user_id}`);
    const result = await db.insertPost(req.body.title, req.body.content, req.body.user_id);

    res.send({cnt: result.affectedRows});
});


router.put('/update', async (req, res) => {
    console.log(`free board update id:: ${req.body.boardId}`);
    let result = await db.updatePost(req.body.boardId, req.body.user_id, req.body.title, req.body.content);

    res.send({cnt: result.affectedRows});
});


router.delete('/delete/:userId/:boardId', async (req, res) => {
    console.log(`free board delete id:: ${req.params}`);
    const result = await db.deletePost(req.params.boardId, req.params.userId);
    
    res.send({cnt: result.affectedRows});
});


router.put('/read', async (req, res) => {
    console.log(`add freeboard read cnt ${req.body.boardId}`);
    await db.addReadCnt(req.body.boardId);
});


router.put('/report', async (req, res) => {
    console.log(`report freeboard ${req.body.boardId}`);
    await db.reportPost(req.body.boardId);
});

module.exports = router;