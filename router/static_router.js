const express = require('express');
const router = express.Router();


router.get('/privacy_policy', async (req, res) => {
    res.render('privacy_policy.html');
});


module.exports = router;