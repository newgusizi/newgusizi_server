var express = require('express');
const db = require('../db/db_connect');
var router = express.Router();


/**
 * 최근 7일치만
 */
router.post('/info', async (req, res) => {
    const search_name = req.body.search_name;
    const type = (req.body.cat.includes('CPU'))?1:2;
    console.log('request info list');
    console.log(`search name:: ${search_name} / type :: ${type}`);

    const maxAndMinResult = await db.getMaxAndMin(search_name, type);
    const priceList = await db.getPriceList(search_name, type);

    res.send({
        max: maxAndMinResult.MAX,
        min: maxAndMinResult.MIN,
        list: priceList
    });
});



module.exports = router;