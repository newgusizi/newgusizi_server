const express = require('express');
const db = require('../db/db_connect');
const router = express.Router();


/**
 * return JSON({cnt:Int})
 */
 router.post('/connect', async (req, res) => {
    const id = req.body.user_id;
    const os_name = req.body.os_name;
    const cpu = req.body.cpu;
    const device = req.body.device;
    const os_version = req.body.os_version;
    console.log(`login history id :: ${id} / ${os_name} / ${cpu} / ${device} / ${os_version}`);
    try {
        result = await db.insertLoginHist(id, os_name, cpu, device, os_version);
        console.log(result);
    } catch(e) {
        console.log(`connect info fail :: ${id}`);
    }
});


module.exports = router;