const express = require('express');
const db = require('../db/db_connect');
const router = express.Router();


router.post('/', async (req, res) => {
    console.log(`myinfo go, id : ${req.body.user_id}`);
    const result = await db.getMyInfo(req.body.user_id);
    res.send(result);
});

module.exports = router;