var express = require('express');
const db = require('../db/db_connect');
var router = express.Router();


router.post('/cat', async (req, res) => {
    const type = (req.body.cat.includes('CPU'))?1:2;
    console.log('request cat list');
    console.log(`cpu name :: ${type}`);
    let result = await db.getCat(type);

    res.send(result);
});


/**
 * 최근 7일치만
 */
router.use('/bottomCat', (req, res) => {
    console.log(`send bottom categories`);
    const result = [
        {'idx': 0, 'cat_name':'GPU', 'icon_path': 'assets/bottom_nav_icons/gpu.svg'},
        {'idx': 1, 'cat_name':'CPU', 'icon_path': 'assets/bottom_nav_icons/cpu.svg'},
        {'idx': 2, 'cat_name':'BOARD', 'icon_path': 'assets/bottom_nav_icons/board.svg'},
        {'idx': 3, 'cat_name':'내정보', 'icon_path': 'assets/bottom_nav_icons/info.svg'}
    ];
    res.send(result);
});



module.exports = router;