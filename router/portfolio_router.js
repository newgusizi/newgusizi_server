const express = require('express');
const router = express.Router();


router.get('/', async (req, res) => {
    console.log('포트폴리오 열람!');
    console.log(new Date().toLocaleString());
    res.render('portfolio.html');
});


module.exports = router;