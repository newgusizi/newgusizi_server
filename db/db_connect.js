const mysql2 = require('mysql2');
const connectionLimit = 10;


console.log('connect DB info');
console.log(`HOST:: ${process.env.DB_HOST}`);
console.log(`DB_USER:: ${process.env.DB_USER}`);
console.log(`DB_PASSWORD:: ${process.env.DB_PASSWORD}`);
console.log(`DB_SCHEME:: ${process.env.DB_SCHEME}`);

// 옵션이 제대로 들어갔지만 비밀번호 사용x면 gcp 방화벽이나 mysql 접속계정의 ip업데이트
const pool = mysql2.createPool({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_SCHEME,
    connectionLimit: connectionLimit,
    waitForConnections: true,
});

const promisePool = pool.promise();

const priceLimit = 7;
const boardLimit = 6;


module.exports.getMyInfo = async function(user_id) {
    const conn = await promisePool.getConnection();
    try {
        const query = `SELECT 	user_id,
                                user_pw,
                                reg_id,
                                DATE_FORMAT(reg_date, '%Y.%m.%d') as reg_date,
                                user_phone,
                                user_name,
                                write_post,
                                write_comment
                        FROM    newgusizi.member 
                        WHERE   user_id = '${user_id}';`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('getMyInfo Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log();
    }
}


// TODO:: add exception throw class and encrypte lib
module.exports.regMember = async function(user_id, user_pw, user_phone, user_name, user_key) {
    const conn = await promisePool.getConnection();
    try {
        // add logic enc pw
        const query = `insert into member(user_id, user_pw, reg_date, user_phone, user_name, user_key)
                                            values ('${user_id}','${user_pw}', now(), '${user_phone}', '${user_name}', '${user_key}');`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('regMember Exception.');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.checkId = async function(user_id) {
    const conn = await promisePool.getConnection();
    try {
        const query = `SELECT count(*) as cnt FROM newgusizi.member WHERE user_id = '${user_id}';`;                               
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('checkId Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.getKey = async function(user_id) {
    const conn = await promisePool.getConnection();
    try {
        const query = `SELECT user_key FROM newgusizi.member WHERE user_id = '${user_id}';`;    
        console.log(`query:: ${query}`);                           
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('checkId Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.login = async function(user_id, user_pw) {
    const conn = await promisePool.getConnection();

    try {
        // add logic enc pw
        const query = `SELECT count(*) as cnt, user_id, user_name, user_state FROM newgusizi.member 
                        WHERE user_id='${user_id}'
                        AND   user_pw='${user_pw}';`;
        const result = (await conn.query(query))[0][0];
        console.log(`query:: ${query}`);    
        return result;
    } catch(e) {
        console.log('login Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.block_user = async function(user_id, block_user_id) {
    const conn = await promisePool.getConnection();

    try {
        // add logic enc pw
        const query = `INSERT INTO newgusizi.block_user_list(user_id, block_user_id) 
                        values('${user_id}', '${block_user_id}');`;
        const result = (await conn.query(query))[0];
        console.log(`query:: ${query}`);    
        return result;
    } catch(e) {
        console.log('login Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.addWritePostCnt = async function(user_id) {
    const conn = await promisePool.getConnection();

    try {
        // add logic enc pw
        const query  = `UPDATE newgusizi.member
                        SET write_post = write_post+1
                        WHERE user_id = '${user_id}';`;
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('login Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.addWriteCommentCnt = async function(user_id) {
    const conn = await promisePool.getConnection();

    try {
        // add logic enc pw
        const query  = `UPDATE newgusizi.member
                        SET write_comment = write_comment+1
                        WHERE user_id = '${user_id}';`;
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('login Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.getCat = async function(type) {
    const conn = await promisePool.getConnection();

    try {
        // add logic enc pw
        const query =  `SELECT search_name, type  
                        FROM newgusizi.search
                        WHERE type = ${type}`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('getCat Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.getMaxAndMin = async function(search_name, type) {
    const conn = await promisePool.getConnection();

    try {
        const query = ` SELECT  max(price) as MAX,
                                min(price) as MIN 
                        FROM (  SELECT * 
                                FROM newgusizi.prices 
                                WHERE search_name = '${search_name}' 
                                AND type = ${type} 
                                ORDER BY idx DESC 
                                LIMIT ${priceLimit}) prices; `;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('getMaxAndMin Exception');
        console.log(e);
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.getPriceList = async function(search_name, type) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `SELECT price,
                               DATE_FORMAT(update_date, '%Y%m%d') as date,
                               market
                        FROM newgusizi.prices
                        WHERE search_name = '${search_name}'
                        AND type=${type}
                        ORDER BY date DESC
                        LIMIT ${priceLimit};`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('getPriceList Exception');
        console.log(e);
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}



module.exports.boardList = async function(page) {
    const conn = await promisePool.getConnection();

    try {
        const offset = (page-1)*boardLimit;
        const query = ` SELECT 	board.boardId as boardId,
                                board.title as title,
                                board.content as content,
                                IF(DATE_FORMAT(reg_time, '%Y.%m.%d') = DATE_FORMAT(now(), '%Y.%m.%d'), DATE_FORMAT(board.reg_time, '%T'), DATE_FORMAT(board.reg_time, '%Y.%m.%d')) as reg_time,
                                board.read_cnt as read_cnt,
                                board.user_id as user_id
                        FROM newgusizi.board board
                        LEFT OUTER JOIN newgusizi.block_user_list block_list
                        ON board.user_id = block_list.block_user_id
                        WHERE block_list.block_user_id IS null
                        ORDER BY boardId DESC
                        LIMIT ${boardLimit} OFFSET ${offset};`

        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('boardList Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.boardDetail = async function(boardId) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `SELECT boardId, 
                               title,
                               content,
                               DATE_FORMAT(reg_time, '%Y.%m.%d  %T') as reg_time,
                               user_id
                        FROM newgusizi.board
                        WHERE boardId = ${boardId};`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('searchboard Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.insertPost = async function(title, content, user_id) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `INSERT INTO newgusizi.board (title, content, reg_time, user_id) 
                        VALUES ('${title}', '${content}', now(), '${user_id}');`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('insertPost Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.deletePost = async function(board_id, user_id) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `DELETE FROM newgusizi.board 
                        WHERE boardId = ${board_id}
                        AND user_id = '${user_id}';`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('deletePost Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.updatePost = async function(boardId, user_id, title, content) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `UPDATE newgusizi.board 
                        SET title = '${title}', 
                            content = '${content}', 
                            reg_time = now() 
                        WHERE boardId = ${boardId} 
                        AND user_id = '${user_id}';`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('searchboard Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.addReadCnt = async function(boardId) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `UPDATE newgusizi.board 
                        SET read_cnt = read_cnt+1
                        WHERE boardId = ${boardId};`;

        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('searchboard Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.reportPost = async function(boardId) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `UPDATE newgusizi.board 
                        SET block_cnt = block_cnt+1
                        WHERE boardId = ${boardId};`;

        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('reportPost Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.insertComment = async function(boardId, user_id, comment) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `INSERT 	INTO newgusizi.comment (boardId, user_id, comment, reg_time) 
		                VALUES (${boardId}, '${user_id}', '${comment}', now());`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('searchboard Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.commentList = async function(boardId) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `SELECT commentId,
                                boardId,
                                user_id,
                                comment,
                                IF(DATE_FORMAT(reg_time, '%Y.%m.%d') = DATE_FORMAT(now(), '%Y.%m.%d'), DATE_FORMAT(reg_time, '%T'), DATE_FORMAT(reg_time, '%Y.%m.%d')) as reg_time
                        FROM newgusizi.comment
                        WHERE boardId = '${boardId}';`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('searchboard Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.insertLoginHist = async function(user_id, os_name, cpu, device, os_version) {
    const conn = await promisePool.getConnection();

    try {
        const query =  `INSERT INTO newgusizi.login_hist (user_id, os_name, cpu, device, os_version) 
                        VALUES ('${user_id}', '${os_name}', '${cpu}', '${device}', '${os_version}');`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('insertLoginHist Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.checkServerState = async function() {
    const conn = await promisePool.getConnection();

    try {
        const query = ` SELECT state_code,
                               state_msg,
                               host_url
                        FROM newgusizi.server_state 
                        WHERE state_name = 'server';`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0][0];
        return result;
    } catch(e) {
        console.log('checkServerState Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}


module.exports.testFun = async function() {
    const conn = await promisePool.getConnection();

    try {
        const query = `SELECT * FROM newgusizi.search;`;
        console.log(`query:: ${query}`);
        const result = (await conn.query(query))[0];
        return result;
    } catch(e) {
        console.log('testFun Exception');
        console.log(e);
        return e;
    } finally {
        conn.release();
        console.log(new Date().toLocaleString());
    }
}